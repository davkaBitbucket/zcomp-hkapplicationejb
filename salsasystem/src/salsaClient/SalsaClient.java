
package salsaClient;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.AbstractTableModel;

import server.HkSystemDAO;
import server.SalsaEmail;

public class SalsaClient extends JFrame
{
    private JTable tableEmail;
    private JTextField txtSearchEmail;
    private JButton btnSearchEmail;
    private JLabel lblWelcome;
    // EMAILS
    private EmailTableModel emailTableModel;
    private HkSystemDAO hksystemdao;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    SalsaClient frame = new SalsaClient();
                    frame.setVisible(true);
                    System.out.println("SalsaClient started successfully...");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public SalsaClient() throws Exception
    {
        // GET CONTEXT AND LOOKUP HKSYSTEM
        Context ctx = getContext();
        hksystemdao = (HkSystemDAO) ctx.lookup("HkSystemDAOBean/remote");

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 777, 694);

        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("File");
        // Add exit menuItem
        JMenuItem menuItem = new JMenuItem("Exit");
        menuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        menu.add(menuItem);
        menuBar.add(menu);
        setJMenuBar(menuBar);

        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        lblWelcome = new JLabel("Welcome to SALSA system");
        lblWelcome.setFont(new Font("Dialog", Font.BOLD, 22));
        lblWelcome.setBounds(8, 12, 500, 30);
        contentPane.add(lblWelcome);

        JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        tabbedPane.setBounds(5, 60, 758, 550);
        contentPane.add(tabbedPane);


        JPanel panelEmail = new JPanel();
        tabbedPane.addTab("Emails", null, panelEmail, null);
        panelEmail.setLayout(null);

        JScrollPane scrollPaneEmail = new JScrollPane();
        scrollPaneEmail.setBounds(0, 0, 565, 321);
        panelEmail.add(scrollPaneEmail);

        tableEmail = new JTable();
        tableEmail.setAutoCreateRowSorter(true);

        // Only single selection is allowed
        tableEmail.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        // _____ Get tableModel from server and set it _____//
        List<SalsaEmail> emails = hksystemdao.getAllSalsaEmails();
        emailTableModel = new EmailTableModel();
        emailTableModel.setEmailTableModel(emails);

        tableEmail.setModel(emailTableModel);
        // Set minimum column width
        tableEmail.getColumnModel().getColumn(0).setMinWidth(210);
        tableEmail.getColumnModel().getColumn(1).setMinWidth(110);

        // Set default selection
        tableEmail.requestFocus();
        tableEmail.changeSelection(0, 0, false, false);

        scrollPaneEmail.setViewportView(tableEmail);

        JButton btnOpenEmail = new JButton("Open");
        btnOpenEmail.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                int row = tableEmail.getSelectedRow();

                if (row == -1) {
                    JOptionPane.showMessageDialog(new JFrame(),
                            "Please select an email!");
                } else {
                    EmailFrame emailFrame = new EmailFrame();
                    SalsaEmail tmpEmail = emailTableModel.getEmailAt(row);

                    emailFrame.setTxtLblFrom(tmpEmail.getSendFrom());
                    emailFrame.setTxtLblTo(tmpEmail.getSendTo());
                    emailFrame.setTxtLblSubject(tmpEmail.getSubject());
                    emailFrame.setLblDate(DateFormat.getInstance().format(
                            tmpEmail.getDate()));
                    emailFrame.setTextAreaMessage(tmpEmail.getMessage());
                    emailFrame.setVisible(true);
                }
            }
        });
        btnOpenEmail.setBounds(0, 321, 117, 25);
        panelEmail.add(btnOpenEmail);

        txtSearchEmail = new JTextField();
        txtSearchEmail.setBounds(230, 321, 200, 26);
        panelEmail.add(txtSearchEmail);
        txtSearchEmail.setColumns(10);

        btnSearchEmail = new JButton("Search");
        btnSearchEmail.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String txtSearch = txtSearchEmail.getText().toLowerCase();
                List<SalsaEmail> tmpEmails = hksystemdao.searchSalsaEmails(txtSearch);
                emailTableModel.setEmailTableModel(tmpEmails);
                emailTableModel.fireTableDataChanged();
            }
        });

        txtSearchEmail.getInputMap().put(
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "check");

        txtSearchEmail.getActionMap().put("check", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                btnSearchEmail.doClick();
            }
        });

        btnSearchEmail.setBounds(446, 321, 117, 25);
        panelEmail.add(btnSearchEmail);

    }

    // CREATE CONTEXT
    private static String serverURL = "localhost";

    private static Context getContext() throws Exception {
        Properties p = new Properties();
        p.put(Context.INITIAL_CONTEXT_FACTORY,
                "org.jnp.interfaces.NamingContextFactory");
        p.put(Context.URL_PKG_PREFIXES, "org.jboss.naming:org.jnp.interfaces");
        p.put(Context.PROVIDER_URL, serverURL);
        Context ctx = new InitialContext(p);
        return ctx;
    }
} // /////////////////////////// END OF CLIENT CLASS

// EMAIL FRAME CLASS
class EmailFrame extends JFrame
{
    private JPanel contentPane;
    private JLabel lblFrom;
    private JLabel lblTo;
    private JLabel lblSubject;
    private JTextArea textAreaMessage;
    private JLabel lblDate;
    private JButton btnClose;

    public EmailFrame()
    {
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setBounds(100, 100, 580, 654);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        lblFrom = new JLabel("From:  ");
        lblFrom.setBounds(12, 49, 332, 15);
        contentPane.add(lblFrom);

        lblTo = new JLabel("To:  ");
        lblTo.setBounds(12, 96, 332, 15);
        contentPane.add(lblTo);

        lblSubject = new JLabel("Subject:  ");
        lblSubject.setBounds(12, 170, 550, 15);
        contentPane.add(lblSubject);

        textAreaMessage = new JTextArea();
        textAreaMessage.setBounds(12, 203, 556, 320);
        textAreaMessage.setLineWrap(true);
        contentPane.add(textAreaMessage);

        lblDate = new JLabel("Date:  ");
        lblDate.setBounds(413, 96, 155, 15);
        contentPane.add(lblDate);

        btnClose = new JButton("Close");
        btnClose.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        btnClose.setBounds(12, 563, 117, 25);
        contentPane.add(btnClose);
    }

    public void setTxtLblFrom(String txtFrom) {
        lblFrom.setText("From:  " + txtFrom);
    }

    public void setTxtLblTo(String txtTo) {
        lblTo.setText("To:  " + txtTo);
    }

    public void setTxtLblSubject(String txtSubject) {
        lblSubject.setText("Subject:  " + txtSubject);
    }

    public void setLblDate(String txtDate) {
        lblDate.setText("Date:  " + txtDate);
    }

    public void setTextAreaMessage(String txtAreaMessage) {
        textAreaMessage.setText(txtAreaMessage);
    }
} // END OF EMAIL FRAME CLASS

// EMAIL TABLE MODEL
class EmailTableModel extends AbstractTableModel
{
    private List<SalsaEmail> modelEmails;

    public EmailTableModel()
    {
    }

    public void setEmailTableModel(List<SalsaEmail> theEmails) {
        modelEmails = theEmails;
    }

    String[] columnNames = { "Subject", "From", "Date", };

    public String getColumnName(int col) {
        return columnNames[col];
    }

    @Override
    public int getRowCount() {
        return modelEmails.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object value = "?";
        SalsaEmail email = modelEmails.get(rowIndex);
        switch (columnIndex) {
        case 0:
            value = email.getSubject();
            break;
        case 1:
            value = email.getSendFrom();
            break;
        case 2:
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            value = sdf.format(email.getDate());
            break;
        }
        return value;
    }

    public SalsaEmail getEmailAt(int row) {
        return modelEmails.get(row);
    }
}
