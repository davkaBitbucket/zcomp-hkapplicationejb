package salsaServer;

import java.util.Properties;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;

import server.Email;
import server.HkBatchEmail;
import server.HkSystem;
import server.SalsaEmail;

@MessageDriven(activationConfig = {
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "destination", propertyValue = "queue/hkapplication/emails") })
public class SalsaDataReceiver implements MessageListener
{
    public void onMessage(Message recvMsg) {
        try {
            Context ctx = getContext();
            HkSystem hksystem = (HkSystem)ctx.lookup("HkSystemBean/remote");
            if (recvMsg instanceof ObjectMessage) {
                ObjectMessage emailobj = (ObjectMessage) recvMsg;
                HkBatchEmail emails = (HkBatchEmail) emailobj.getObject();
                Email tmp;
                for (int i = 0; i<emails.size(); i++) {
                    tmp = emails.get(i);
                    SalsaEmail tmpSalsaEmail = new SalsaEmail(tmp.getSendFrom(), tmp.getSendFrom(), tmp.getDate(), tmp.getSendTo(),
                            tmp.getMessage());
                    System.out.println("<SALSA-ALERT: RECEIVING " + i + " EMAILS");
                    hksystem.persistObject(tmpSalsaEmail);
                }
                System.out.println("<SALSA-ALERT: RECEIVED ALL THE EMAILS");
            }
            if (recvMsg instanceof TextMessage) {
                TextMessage txtMsg = (TextMessage) recvMsg;
                String strText = txtMsg.getText();
                System.out.println("<SALSA-ALERT: RECEIVED TEXT MESSAGE FROM HK SAYING\n" + strText);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public Context getContext() throws Exception {
        Context ctx = null;
        Properties p = new Properties();
        p.put(Context.INITIAL_CONTEXT_FACTORY,
                "org.jnp.interfaces.NamingContextFactory");
        p.put(Context.URL_PKG_PREFIXES, "org.jboss.naming:org.jnp.interfaces");
        p.put(Context.PROVIDER_URL, "localhost");
        ctx = new InitialContext(p);
        return ctx;
    }
}