package server;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "SALSA_EMAIL")
public class SalsaEmail implements Serializable
{
    private int id;
    private String sendFrom;
    private String sendTo;
    private Date date;
    private String subject;
    private String message;
    
    public SalsaEmail() {

    }

    public SalsaEmail(String subject, String sendFrom, Date aDate, String sendTo,
            String message) {
        this.subject = subject;
        this.sendFrom = sendFrom;
        date = aDate;
        this.sendTo = sendTo;
        this.message = message;
    }
    
    @Id 
    @GeneratedValue(strategy=GenerationType.AUTO)
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getSendFrom() {
        return sendFrom;
    }
    public void setSendFrom(String sendFrom) {
        this.sendFrom = sendFrom;
    }
    public String getSendTo() {
        return sendTo;
    }
    public void setSendTo(String sendTo) {
        this.sendTo = sendTo;
    }
    public Date getDate() {
        return date;
    }
    public void setDate(Date date) {
        this.date = date;
    }
    public String getSubject() {
        return subject;
    }
    public void setSubject(String subject) {
        this.subject = subject;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
}
