package server;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class HkBatchEmail implements Serializable
{
    private List<Email> emails;
    
    public HkBatchEmail() {
        emails = new ArrayList<Email>();
    }

    public List<Email> getEmails() {
        return emails;
    }

    public void setEmails(List<Email> emails) {
        this.emails = emails;
    }
    
    public int size() {
        return emails.size();
    }
    
    public Email get(int index) {
        return emails.get(index);
    }
}
