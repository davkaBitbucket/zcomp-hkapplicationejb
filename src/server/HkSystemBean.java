package server;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.ejb.Local;
import javax.ejb.Remote;
import javax.ejb.Stateful;
import javax.jms.ObjectMessage;
import javax.jms.Queue;
import javax.jms.QueueConnection;
import javax.jms.QueueConnectionFactory;
import javax.jms.QueueSender;
import javax.jms.QueueSession;
import javax.jms.TextMessage;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Stateful
@Remote(HkSystem.class)
@Local
public class HkSystemBean implements HkSystem, java.io.Serializable {

	@PersistenceContext
	private EntityManager manager;

	public void createDummyData() {
		// CREATE EMPLOYEES
		// 1 EMPLOYEE
		String name = "James Brown";
		String address = "110 Fox street, Lee";
		String profile = "I arrived in Indonesia in 1991 to work as a teacher in an English language school. By 1998 I was managing the language division of a corporate training company, providing in-house language and business skills to a variety of industries. I decided to head back to the UK when the economy crisis hit, however, the day before I was due to leave I got a job as QA Manager with Bakrie Kvaerner Engineering (BKE), a small pipeline design company. I subsequently worked as Project Services Manager and Proposals Manager. AMEC acquired BKE and its JV partner Berca in 2001. You could say I came as ‘part of the package’. I worked in business development and proposals before being appointed General Manager in 2007.";
		String password = "pass";
		Employee employee1 = new Employee(name, address, profile, password);
		manager.persist(employee1);

		// 2 EMPLOYEE
		name = "John Smith";
		address = "120 Hill Road, Oxford";
		profile = "I joined AMEC about 8 years ago as a graduate, having attained a degree in Aerospace Engineering. Before deciding which company to join, I researched a number of engineering organisations and opted for AMEC as I genuinely felt it was the most interesting and enticing of all the companies. The financial rewards at AMEC were also greater than the competition. What also attracted me was the diversity of the projects I could potentially get involved in, as well as the sheer size and scale of the projects and the international work that AMEC is renowned for. My aerospace engineering degree came in very useful for the next two years with AMEC as I was invited to join a group called Advanced Engineering Services where I had the opportunity to work on some cutting edge projects.";
		password = "pass";
		Employee employee2 = new Employee(name, address, profile, password);
		manager.persist(employee2);

		// 3 EMPLOYEE
		name = "Thomas Miller";
		address = "45 Cark Road, Millhill";
		profile = "My day invariably starts with a pit stop at Tim Horton’s for a medium coffee (two cream, one sugar). There are several coffee shops nearby to choose from, but I just can’t seem to wean myself off the 18% cream! I normally come into the office at 10 AM, but will occasionally start work in the wee hours of the morning if there is a particular project I’m working on that requires more attention. In which case, better make that a large coffee… What happens throughout the day? My time is split between project management and technical work. I am involved in defining project scope, developing budgets and schedules, preparing work plans to determine how to execute the work, and confirming that appropriate QA procedures are followed. On a day to day basis, I assist staff working on my projects by ensuring that they have the information, processes and guidance they need to do a quality job efficiently. All this really boils down to is emailing, making phone calls, interfacing with other work groups, and determining what to do and how to do it, but somehow I feel useful knowing that I’ve made things go smoother! I am also involved in doing the technical work on some of my projects. This provides a good reality check on the roadblocks that are often encountered at the working level that have to be taken into consideration at the project management level. Once or twice a month I travel to Bruce nuclear power plant in Tiverton for client meetings or to work with site staff.";
		password = "pass";
		Employee employee3 = new Employee(name, address, profile, password);
		manager.persist(employee3);

		// 4 EMPLOYEE
		name = "Linda Moore";
		address = "78 Belmont hill, Lewisham";
		profile = "I read on the news that AMEC intended to expand its business presence in this long-term growth market. I recognized that it is a good opportunity for AMEC and for me. I also recognized that my cross-disciplinary background (from engineer to sales and business development executive, to financial advisor, to Head of Energy Project of an investment company) could make a meaningful contribution to AMEC success in the capital intensive sectors that AMEC serves. Hence, I sent my CV to AMEC..";
		password = "pass";
		Employee employee4 = new Employee(name, address, profile, password);
		manager.persist(employee4);

		// CREATE EMAILS
		// 1 EMAIL
		String to = "info@hk.org";
		String subject = "Project description from EThames";
		String from = "sale@ethames.com";
		String message = "blablablabla";
		Email email = new Email(subject, from, customDate("2011-01-18 12:30"),
				to, message);
		manager.persist(email);

		// 2 EMAIL
		to = "dava@hk.org";
		subject = "Product Invoice Notification for Tuesday";
		from = "tech@stanley.com";
		message = "Dear Dava,\n\nFar far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean. A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth. Even the all-powerful Pointing has no control about the blind texts it is an almost unorthographic life One day however a small line of blind text by the name of Lorem Ipsum decided to leave for the far World of Grammar. The Big Oxmox advised her not to do so, because there were thousands of bad Commas, wild Question Marks and devious Semikoli, but the Little Blind Text didn’t listen. She packed her seven versalia, put her initial into the belt and made herself on the way. When she reached the first hills of the Italic Mountains, she had a last view back on the skyline of her hometown Bookmarksgrove, the headline of Alphabet Village and the subline of her own road, the Line Lane.\n\n Regards,\nJohn";
		email = new Email(subject, from, customDate("2012-03-09 09:44"), to,
				message);
		manager.persist(email);

		// 3 EMAIL
		to = "mike@hk.org";
		subject = "Product fixed price listing";
		from = "sale@sony.com";
		message = "Dear Mike,\n\nThis is wonderful serenity has taken possession of my entire soul, like these sweet mornings of spring which I enjoy with my whole heart. I am alone, and feel the charm of existence in this spot, which was created for the bliss of souls like mine. I am so happy, my dear friend, so absorbed in the exquisite sense of mere tranquil existence, that I neglect my talents. I should be incapable of drawing a single stroke at the present moment; and yet I feel that I never was a greater artist than now. When, while the lovely valley teems with vapour around me, and the meridian sun strikes the upper surface of the impenetrable foliage of my trees, and but a few stray gleams steal into the inner sanctuary, I throw myself down among the tall grass by the trickling stream; and, as I lie close to the earth, a thousand unknown plants are noticed by me: when I hear the buzz of the little world among the stalks, and grow familiar with the countless indescribable forms of the insects and flies, then I feel the presence of the Almighty, who formed us in his own image.\n\nRegards,\nKevin";
		email = new Email(subject, from, customDate("2013-01-15 10:17"), to,
				message);
		manager.persist(email);

		// 4 EMAIL
		to = "john@hk.org";
		subject = "Technical inspection this weekend";
		from = "tech@microsoft.com";
		message = "Dear John,\n\n One morning, when Gregor Samsa woke from troubled dreams, he found himself transformed in his bed into a horrible vermin. He lay on his armour-like back, and if he lifted his head a little he could see his brown belly, slightly domed and divided by arches into stiff sections. The bedding was hardly able to cover it and seemed ready to slide off any moment. His many legs, pitifully thin compared with the size of the rest of him, waved about helplessly as he looked. What's happened to me? he thought. It wasn't a dream. His room, a proper human room although a little too small, lay peacefully between its four familiar walls. A collection of textile samples lay spread out on the table - Samsa was a travelling salesman - and above it there hung a picture that he had recently cut out of an illustrated magazine and housed in a nice, gilded frame. It showed a lady fitted out with a fur hat and fur boa who sat upright, raising a heavy fur muff that covered the whole of her lower arm towards the viewer. Gregor then turned to look out the window at the dull weather. Drops \n\nRegards,\nDavid";
		email = new Email(subject, from, customDate("2005-06-23 11:58"), to,
				message);
		manager.persist(email);

		// CREATE DUMMY BEENZ POINTS
		// 1 BEENZ
		Beenz beenz1 = new Beenz(80, "System", customDate("2010-01-23 11:58"),
				employee1);
		manager.persist(beenz1);

		// 2 BEENZ
		Beenz beenz2 = new Beenz(50, "System", customDate("2011-02-23 11:58"),
				employee2);
		manager.persist(beenz2);

		// 3 BEENZ
		Beenz beenz3 = new Beenz(100, "System", customDate("2012-03-23 11:58"),
				employee3);
		manager.persist(beenz3);

		// 4 BEENZ
		Beenz beenz4 = new Beenz(0, "System", customDate("2013-10-23 11:58"),
				employee4);
		manager.persist(beenz4);
	}

	public Employee createEmployee(String aName, String anAddress,
            String aProfile, String aPassword) {
        Employee employee = new Employee(aName, anAddress, aProfile, aPassword);
        manager.persist(employee);
        Beenz beenz = new Beenz(0, "System", new Date(), employee);
        manager.persist(beenz);
        System.out.println("<HK-ALERT: AN EMPLOYEE CREATED SUCCESSFULLY");
        return employee;
    }
	
	public void merge(Object anObject) {
		manager.merge(anObject);
		System.out.println("<HK-ALERT: EMPLOYEE EDITED SUCCESSFULLY");
	}

	public void remove(int anId) {
		String qry = "SELECT e FROM Employee e WHERE e.id=" + anId;
		Employee tmpEmployee = (Employee) manager.createQuery(qry)
				.getSingleResult();
		manager.remove(tmpEmployee);
		System.out.println("<HK-ALERT: EMPLOYEE REMOVED SUCCESSFULLY");
	}

	public void createBeenz(Beenz aBeenz) {
		manager.persist(aBeenz);
		System.out.println("New beenz data created!!!");
	}

	public static Date customDate(String dateString) {
		Date date = new Date();
		try {
			date = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(dateString);
		} catch (Exception pe) {
			System.out.println("ERROR: could not parse date in string \""
					+ dateString + "\"");
			pe.printStackTrace();
		}
		return date;
	}

	public void sendDataToSalsa() {
		QueueConnection cnn = null;
		QueueSender sender = null;
		QueueSession session = null;
		Properties p = new Properties();
		p.put(Context.INITIAL_CONTEXT_FACTORY,"org.jnp.interfaces.NamingContextFactory");
		p.put(Context.URL_PKG_PREFIXES, "org.jboxx.naming:org.jnp.interfaces");
		p.put(Context.PROVIDER_URL, "localhost");
		try {
			InitialContext ctx = new InitialContext(p);
			Queue queue = (Queue) ctx.lookup("queue/hkapplication/emails");
			QueueConnectionFactory factory = (QueueConnectionFactory) ctx.lookup("ConnectionFactory");
			cnn = factory.createQueueConnection();
			session = cnn.createQueueSession(false, QueueSession.AUTO_ACKNOWLEDGE);
			sender = session.createSender(queue);
			// SEND ALL THE EMAILS OBJECT MESSAGE
			List<Email> emails = getAllEmails();
            HkBatchEmail batchEmail = new HkBatchEmail();
            batchEmail.setEmails(emails);
            ObjectMessage objMsg = session.createObjectMessage(batchEmail);
			sender.send(objMsg);
			// SEND JUST TEXT MESSAGE
            TextMessage txtMsg = session.createTextMessage("SENDING YOU ALL THE EMAILS HOPE YOU GET THEM ALL");
            sender.send(txtMsg);
			System.out.println("<HK-ALERT: ALL EMAILS SENT TO SALSA SYSTEM>");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	
	public List<Email> getAllEmails() {
        String qry = "SELECT em FROM Email em";              
        @SuppressWarnings("unchecked")
        List<Email> emails =  manager.createQuery(qry).getResultList();            
        return emails;        
    }
	
	public void persistObject(Object anObject) {
	    manager.persist(anObject);
	}
}
