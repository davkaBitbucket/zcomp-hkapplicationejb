package server;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "BEENZ")
public class Beenz implements Serializable{
	private int id;
	private int points;
	private String authorisedBy;
	private Date date;
	private Employee employee;
	
	public Beenz() {
		
	}
	
	public Beenz(int thePoints, String anAuthorisedBy, Date aDate, Employee anEmployee) {
        points = thePoints;
        authorisedBy = anAuthorisedBy;
        date = aDate;
        employee = anEmployee;
    }
	
	@Id @GeneratedValue(strategy=GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	@ManyToOne
	@JoinColumn(name = "emp_id")
    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }
    
    public Date getDate() {
        return date;
    }
    
    public void setDate(Date date) {
        this.date = date;
    }
    
    public String getAuthorisedBy() {
        return authorisedBy;
    }

    public void setAuthorisedBy(String authorisedBy) {
        this.authorisedBy = authorisedBy;
    }

}
