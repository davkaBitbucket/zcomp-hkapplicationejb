
package server;


import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "EMPLOYEE")
public class Employee implements Serializable{
	private int id;
	private String name;
	private String address;
	private String profile;
	private String password;
	private Collection<Beenz> beenzs;

	public Employee() {

	}
	
	public Employee(String aName, String anAddress, String aProfile, String aPassword) {
		name = aName;
		address = anAddress;
		profile = aProfile;
		password = aPassword;
	}
	
	public void setEmployee(String aName, String anAddress, String aProfile, String aPassword) {
        name = aName;
        address = anAddress;
        profile = aProfile;
        password = aPassword;
    }
	
	@Id 
	@GeneratedValue(strategy=GenerationType.AUTO)
	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getAddress() {
		return address;
	}

	public String getProfile() {
		return profile;
	}
	
	public void setId(int anId) {
		id = anId;
	}

	public void setName(String aName) {
		name = aName;
	}

	public void setAddress(String anAddress) {
		address = anAddress;
	}

	public void setProfile(String aProfile) {
		profile = aProfile;
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy="employee")
	public Collection<Beenz> getBeenzs() {
        return beenzs;
    }

    public void setBeenzs(Collection<Beenz> beenzs) {
        this.beenzs = beenzs;
    }
}












