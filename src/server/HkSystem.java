package server;

public interface HkSystem {
	public void createDummyData(); 

	public void merge(Object anObject);
	public void remove(int anId);
	public void createBeenz(Beenz aBeenz);
	public void sendDataToSalsa();
	public Employee createEmployee(String aName, String anAddress,
            String aProfile, String aPassword);
	public void persistObject(Object anObject);
}
