package server;

import java.util.List;

public interface HkSystemDAO
{
    public Beenz getLatestBeenz(int anId);
    public List<Employee> searchEmps(String aTxtSearch);
    public List<Email> searchEmails(String aTxtSearch);
    public List<Employee> getAllEmployees();
    public List<Email> getAllEmails();
    public List<SalsaEmail> getAllSalsaEmails();
    public List<SalsaEmail> searchSalsaEmails(String aTxtSearch);
}
