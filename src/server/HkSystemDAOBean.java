package server;

import java.util.List;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


@Stateless
@Remote(HkSystemDAO.class)
public class HkSystemDAOBean
{
    @PersistenceContext
    private EntityManager manager;
    
    public Beenz getLatestBeenz(int anId) {
        String qry = "SELECT b FROM Beenz b WHERE b.date = (SELECT max(date) FROM Beenz where employee=" + anId + ")";
        @SuppressWarnings("unchecked")
        List<Beenz> beenzs = manager.createQuery(qry).getResultList();
        return beenzs.get(0);
    }
    
    public List<Employee> searchEmps(String aTxtSearch) {
        String qry = "SELECT e FROM Employee e WHERE e.id like '%" + aTxtSearch + "%'"
                + "OR LCASE(e.name) like '%" + aTxtSearch + "%'" 
                + "OR LCASE(e.address) like '%" + aTxtSearch + "%'"
                + "OR LCASE(e.profile) like '%" + aTxtSearch + "%'"
                + "OR LCASE(e.password) like '%" + aTxtSearch + "%'";           
        @SuppressWarnings("unchecked")
        List<Employee> employees =  manager.createQuery(qry).getResultList();
        System.out.println("Search found " + employees.size() + " employees");
        return employees;        
    }
    
    public List<Email> searchEmails(String aTxtSearch) {
        String qry = "SELECT em FROM Email em WHERE em.id like '%" + aTxtSearch + "%'"
                + "OR LCASE(em.sendFrom) like '%" + aTxtSearch + "%'" 
                + "OR LCASE(em.sendTo) like '%" + aTxtSearch + "%'"
                + "OR LCASE(em.date) like '%" + aTxtSearch + "%'"
                + "OR LCASE(em.subject) like '%" + aTxtSearch + "%'"
                + "OR LCASE(em.message) like '%" + aTxtSearch + "%'";           
        @SuppressWarnings("unchecked")
        List<Email> emails =  manager.createQuery(qry).getResultList();   
        System.out.println("Search found " + emails.size() + " emails");
        return emails;        
    }
    
    public List<Employee> getAllEmployees() {
        String qry = "SELECT e FROM Employee e";              
        @SuppressWarnings("unchecked")
        List<Employee> employees =  manager.createQuery(qry).getResultList();            
        return employees;        
    }
    
    public List<Email> getAllEmails() {
        String qry = "SELECT em FROM Email em";              
        @SuppressWarnings("unchecked")
        List<Email> emails =  manager.createQuery(qry).getResultList();            
        return emails;        
    }
    
    // SALSA EMAIL METHODS
    public List<SalsaEmail> getAllSalsaEmails() {
        String qry = "SELECT em FROM SalsaEmail em";              
        @SuppressWarnings("unchecked")
        List<SalsaEmail> emails =  manager.createQuery(qry).getResultList();            
        return emails;        
    }
    
    public List<SalsaEmail> searchSalsaEmails(String aTxtSearch) {
        String qry = "SELECT em FROM SalsaEmail em WHERE em.id like '%" + aTxtSearch + "%'"
                + "OR LCASE(em.sendFrom) like '%" + aTxtSearch + "%'" 
                + "OR LCASE(em.sendTo) like '%" + aTxtSearch + "%'"
                + "OR LCASE(em.date) like '%" + aTxtSearch + "%'"
                + "OR LCASE(em.subject) like '%" + aTxtSearch + "%'"
                + "OR LCASE(em.message) like '%" + aTxtSearch + "%'";           
        @SuppressWarnings("unchecked")
        List<SalsaEmail> emails =  manager.createQuery(qry).getResultList();   
        System.out.println("<SALSA-ALERT: SEARCH FOUND " + emails.size() + " EMAILS");
        return emails;        
    }
}
