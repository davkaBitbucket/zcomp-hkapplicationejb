package client;

import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.swing.AbstractAction;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.table.AbstractTableModel;

import server.Beenz;
import server.Email;
import server.Employee;
import server.HkSystem;
import server.HkSystemDAO;

public class Client extends JFrame
{

    private JTable tableEmail, tableEmployee;
    private JTextField txtSearchEmail, txtSearchEmp;
    private JButton btnSearchEmail, btnSearchEmp;
    private JLabel lblWelcome;
    // EMPLOYEES
    private EmployeeTableModel empTableModel;
    private List<Employee> employees;
    // EMAILS
    private EmailTableModel emailTableModel;
    private HkSystem hksystem;
    private HkSystemDAO hksystemdao;

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                	Client frame = new Client();
                    frame.setVisible(true);
                    System.out.println("Client started successfully...");
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public Client() throws Exception
    {
        // GET CONTEXT AND LOOKUP HKSYSTEM
        Context ctx = getContext();
        hksystem = (HkSystem) ctx.lookup("HkSystemBean/remote");
        hksystemdao = (HkSystemDAO)ctx.lookup("HkSystemDAOBean/remote");
        // CREATE DUMMY DATA
        hksystem.createDummyData();
        // GET ALL EMPLOYEES AND SET IT
        employees = hksystemdao.getAllEmployees();

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 777, 694);

        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu("File");
        // Add exit menuItem
        JMenuItem menuItem = new JMenuItem("Exit");
        menuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });
        menu.add(menuItem);
        menuBar.add(menu);
        setJMenuBar(menuBar);

        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);

        lblWelcome = new JLabel("Welcome to HK system");
        lblWelcome.setFont(new Font("Dialog", Font.BOLD, 22));
        lblWelcome.setBounds(8, 12, 500, 30);
        contentPane.add(lblWelcome);

        JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
        tabbedPane.setBounds(5, 60, 758, 550);
        contentPane.add(tabbedPane);

        // EMPLOYEE PANEL
        JPanel panelEmployee = new JPanel();
        tabbedPane.addTab("Employees", null, panelEmployee, null);
        panelEmployee.setLayout(null);

        JScrollPane scrollPaneEmp = new JScrollPane();
        scrollPaneEmp.setBounds(0, 0, 565, 321);
        panelEmployee.add(scrollPaneEmp);

        tableEmployee = new JTable();
        tableEmployee.setAutoCreateRowSorter(true);

        // Only single selection is allowed
        tableEmployee.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        // SET EMPLOYEE MODEL TO TABLE MODEL
        empTableModel = new EmployeeTableModel();
        empTableModel.setEmpTableModel(employees);
        tableEmployee.setModel(empTableModel);

        // Set minimum column width
        tableEmployee.getColumnModel().getColumn(0).setMaxWidth(50);
        tableEmployee.getColumnModel().getColumn(1).setMinWidth(110);

        // Set default selection
        tableEmployee.requestFocus();
        tableEmployee.changeSelection(0, 0, false, false);
        scrollPaneEmp.setViewportView(tableEmployee);

        txtSearchEmp = new JTextField();
        txtSearchEmp.setBounds(230, 321, 200, 26);
        panelEmployee.add(txtSearchEmp);
        txtSearchEmp.setColumns(10);

        // Map text field search emp to ENTER key
        txtSearchEmp.getInputMap().put(
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "enter");

        txtSearchEmp.getActionMap().put("enter", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                btnSearchEmp.doClick();
            }
        });

        JButton btnEmpOpen = new JButton("Open");
        btnEmpOpen.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int row = tableEmployee.getSelectedRow();
                if (row == -1) {
                    JOptionPane.showMessageDialog(new JFrame(),
                            "Please select an employee!");
                } else {
                    Employee tmpEmployee = empTableModel.getEmpAt(row);
                    EmployeeFrame employeeFrame = new EmployeeFrame();
                    employeeFrame.setEmpTableModel(empTableModel);
                    employeeFrame.setEmployee(tmpEmployee);
                    employeeFrame.setHkSystem(hksystem);
                    employeeFrame.setEmployees(employees);

                    employeeFrame.setTxtName(tmpEmployee.getName());
                    employeeFrame.setTxtAreaAddress(tmpEmployee.getAddress());
                    employeeFrame.setTxtAreaProfile(tmpEmployee.getProfile());
                    employeeFrame.setTxtPassword(tmpEmployee.getPassword());
                    employeeFrame.setLblWelcomeEmp("Welcome "
                            + tmpEmployee.getName());
                    employeeFrame.setVisible(true);
                }
            }
        });
        btnEmpOpen.setBounds(0, 321, 117, 25);
        panelEmployee.add(btnEmpOpen);

        btnSearchEmp = new JButton("Search");
        btnSearchEmp.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String searchEmp = txtSearchEmp.getText().toLowerCase();
                List<Employee> tmpEmps = hksystemdao.searchEmps(searchEmp);
                empTableModel.setEmpTableModel(tmpEmps);
                empTableModel.fireTableDataChanged();
            }
        });
        btnSearchEmp.setBounds(446, 321, 117, 25);
        panelEmployee.add(btnSearchEmp);

        JButton btnAdd = new JButton("Add");
        btnAdd.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                EmployeeFrame employeeFrame = new EmployeeFrame();
                employeeFrame.setLblWelcomeEmp("Add new Employee");
                employeeFrame.setEmpTableModel(empTableModel);
                employeeFrame.setHkSystem(hksystem);
                employeeFrame.setEmployees(employees);
                employeeFrame.setVisible(true);
            }
        });
        btnAdd.setBounds(0, 370, 117, 25);
        panelEmployee.add(btnAdd);

        JButton btnDelete = new JButton("Delete");
        btnDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int row = tableEmployee.getSelectedRow();
                if (row == -1) {
                    JOptionPane.showMessageDialog(new JFrame(),
                            "Please select an employee!");
                } else {
                    Employee tmpEmployee = empTableModel.getEmpAt(row);
                    hksystem.remove(tmpEmployee.getId());
                    employees.remove(tmpEmployee);
                    empTableModel.setEmpTableModel(employees);
                    empTableModel.fireTableDataChanged();
                }
            }
        });
        btnDelete.setBounds(0, 419, 117, 25);
        panelEmployee.add(btnDelete);

        JButton btnContribute = new JButton("Contribute");
        btnContribute.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int row = tableEmployee.getSelectedRow();
                if (row == -1) {
                    JOptionPane.showMessageDialog(new JFrame(),
                            "Please select an employee!");
                } else {
                    Employee tmpEmployee = empTableModel.getEmpAt(row);
                    Beenz latestBeenz = hksystemdao.getLatestBeenz(tmpEmployee.getId());
                    ContributeFrame contributeFrame = new ContributeFrame();
                    contributeFrame.setBeenz(latestBeenz);
                    contributeFrame.setEmployee(tmpEmployee);
                    contributeFrame.setHkSystem(hksystem);
                    contributeFrame.setVisible(true);
                }
            }
        });
        btnContribute.setBounds(230, 370, 117, 25);
        panelEmployee.add(btnContribute);
        
        JButton btnRedeem = new JButton("Redeem");
        btnRedeem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int row = tableEmployee.getSelectedRow();
                if (row == -1) {
                    JOptionPane.showMessageDialog(new JFrame(),
                            "Please select an employee!");
                } else {
                    Employee tmpEmployee = empTableModel.getEmpAt(row);
                    Beenz latestBeenz = hksystemdao.getLatestBeenz(tmpEmployee.getId());
                    RedeemFrame redeemFrame = new RedeemFrame();
                    redeemFrame.setBeenz(latestBeenz);
                    redeemFrame.setEmployee(tmpEmployee);
                    redeemFrame.setHkSystem(hksystem);
                    redeemFrame.setVisible(true);
                }
            }
        });
        btnRedeem.setBounds(230, 419, 117, 25);
        panelEmployee.add(btnRedeem);
        
        JButton btnTransfer = new JButton("Transfer Data");
        btnTransfer.addActionListener(new ActionListener() {
        	public void actionPerformed(ActionEvent e) {
        		hksystem.sendDataToSalsa();
        	}
        });
        btnTransfer.setBounds(410, 419, 150, 25);
        panelEmployee.add(btnTransfer);

        JPanel panelEmail = new JPanel();
        tabbedPane.addTab("Emails", null, panelEmail, null);
        panelEmail.setLayout(null);

        JScrollPane scrollPaneEmail = new JScrollPane();
        scrollPaneEmail.setBounds(0, 0, 565, 321);
        panelEmail.add(scrollPaneEmail);

        tableEmail = new JTable();
        tableEmail.setAutoCreateRowSorter(true);

        // Only single selection is allowed
        tableEmail.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        // _____ Get tableModel from server and set it _____//
        List<Email> emails = hksystemdao.getAllEmails();
        emailTableModel = new EmailTableModel();
        emailTableModel.setEmailTableModel(emails);

        tableEmail.setModel(emailTableModel);
        // Set minimum column width
        tableEmail.getColumnModel().getColumn(0).setMinWidth(210);
        tableEmail.getColumnModel().getColumn(1).setMinWidth(110);

        // Set default selection
        tableEmail.requestFocus();
        tableEmail.changeSelection(0, 0, false, false);

        scrollPaneEmail.setViewportView(tableEmail);

        JButton btnOpenEmail = new JButton("Open");
        btnOpenEmail.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                int row = tableEmail.getSelectedRow();

                if (row == -1) {
                    JOptionPane.showMessageDialog(new JFrame(),
                            "Please select an email!");
                } else {
                    EmailFrame emailFrame = new EmailFrame();
                    Email tmpEmail = emailTableModel.getEmailAt(row);
                    
                    emailFrame.setTxtLblFrom(tmpEmail.getSendFrom());
                    emailFrame.setTxtLblTo(tmpEmail.getSendTo());
                    emailFrame.setTxtLblSubject(tmpEmail.getSubject());
                    emailFrame.setLblDate(DateFormat.getInstance().format(
                            tmpEmail.getDate()));
                    emailFrame.setTextAreaMessage(tmpEmail.getMessage());
                    emailFrame.setVisible(true);
                }
            }
        });
        btnOpenEmail.setBounds(0, 321, 117, 25);
        panelEmail.add(btnOpenEmail);

        txtSearchEmail = new JTextField();
        txtSearchEmail.setBounds(230, 321, 200, 26);
        panelEmail.add(txtSearchEmail);
        txtSearchEmail.setColumns(10);

        btnSearchEmail = new JButton("Search");
        btnSearchEmail.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String txtSearch = txtSearchEmail.getText().toLowerCase();
                List<Email> tmpEmails = hksystemdao.searchEmails(txtSearch);
                emailTableModel.setEmailTableModel(tmpEmails);
                emailTableModel.fireTableDataChanged();
            }
        });

        txtSearchEmail.getInputMap().put(
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "check");

        txtSearchEmail.getActionMap().put("check", new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                btnSearchEmail.doClick();
            }
        });

        btnSearchEmail.setBounds(446, 321, 117, 25);
        panelEmail.add(btnSearchEmail);

    }

    // CREATE CONTEXT
    private static String serverURL = "localhost";
    private static Context getContext() throws Exception {
        Properties p = new Properties();
        p.put(Context.INITIAL_CONTEXT_FACTORY,
                "org.jnp.interfaces.NamingContextFactory");
        p.put(Context.URL_PKG_PREFIXES, "org.jboss.naming:org.jnp.interfaces");
        p.put(Context.PROVIDER_URL, serverURL);
        Context ctx = new InitialContext(p);
        return ctx;
    }
} // /////////////////////////// END OF CLIENT CLASS

// EMPLOYEES TABLE MODEL
class EmployeeTableModel extends AbstractTableModel
{

    private List<Employee> modelEmployees;

    public EmployeeTableModel()
    {

    }

    public void setEmpTableModel(List<Employee> theEmployees) {
        modelEmployees = theEmployees;
    }

    String[] columnNames = { "Id", "Name", "Address" };

    public String getColumnName(int col) {
        return columnNames[col];
    }

    @Override
    public int getRowCount() {
        return modelEmployees.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object value = "?";
        Employee employee = modelEmployees.get(rowIndex);
        switch (columnIndex) {
        case 0:
            value = employee.getId();
            break;
        case 1:
            value = employee.getName();
            break;
        case 2:
            value = employee.getAddress();
            break;
        }
        return value;
    }

    public Employee getEmpAt(int row) {
        return modelEmployees.get(row);
    }
}

// EMAIL TABLE MODEL
class EmailTableModel extends AbstractTableModel
{

    private List<Email> modelEmails;

    public EmailTableModel()
    {

    }

    public void setEmailTableModel(List<Email> theEmails) {
        modelEmails = theEmails;
    }

    String[] columnNames = { "Subject", "From", "Date", };

    public String getColumnName(int col) {
        return columnNames[col];
    }

    @Override
    public int getRowCount() {
        return modelEmails.size();
    }

    @Override
    public int getColumnCount() {
        return 3;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Object value = "?";
        Email email = modelEmails.get(rowIndex);
        switch (columnIndex) {
        case 0:
            value = email.getSubject();
            break;
        case 1:
            value = email.getSendFrom();
            break;
        case 2:
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            value = sdf.format(email.getDate());
            break;
        }
        return value;
    }

    public Email getEmailAt(int row) {
        return modelEmails.get(row);
    }
}

// EMPLOYEE FRAME CLASS
class EmployeeFrame extends JFrame {
    private JTextField txtName;
    private JTextArea txtAreaAddress;
    private JTextArea txtAreaProfile;
    private JTextField txtPassword;
    private JLabel lblWelcomeEmp;
    private Employee employee;
    private EmployeeTableModel empTableModel;
    private List<Employee> employees;
    private HkSystem hksystem;

    public EmployeeFrame() {
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setBounds(100, 100, 644, 568);
        JPanel contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        
        JPanel panel = new JPanel();
        panel.setBounds(5, 5, 633, 570);
        contentPane.add(panel);
        panel.setLayout(null);
        
        lblWelcomeEmp = new JLabel();
        lblWelcomeEmp.setBounds(0, 0, 450, 25);
        panel.add(lblWelcomeEmp);
        lblWelcomeEmp.setFont(new Font("Dialog", Font.BOLD, 18));
        
        txtAreaProfile = new JTextArea();
        txtAreaProfile.setBounds(290, 50, 320, 400);
        txtAreaProfile.setLineWrap(true);
        panel.add(txtAreaProfile);
        
        JLabel lblName = new JLabel("Name:");
        lblName.setFont(new Font("Dialog", Font.BOLD, 16));
        lblName.setBounds(5, 52, 100, 15);
        panel.add(lblName);
        
        txtName = new JTextField();
        txtName.setFont(new Font("Dialog", Font.PLAIN, 16));
        txtName.setBounds(80, 50, 200, 22);
        panel.add(txtName);
        txtName.setColumns(10);
        
        JLabel lblAdress = new JLabel("Address:");
        lblAdress.setFont(new Font("Dialog", Font.BOLD, 16));
        lblAdress.setBounds(5, 150, 120, 15);
        panel.add(lblAdress);
        
        txtAreaAddress = new JTextArea();
        txtAreaAddress.setFont(new Font("Dialog", Font.PLAIN, 16));
        txtAreaAddress.setLineWrap(true);
        txtAreaAddress.setBounds(100, 150, 180, 100);
        panel.add(txtAreaAddress);
        
        JLabel lblNewLabel = new JLabel("Profile:");
        lblNewLabel.setFont(new Font("Dialog", Font.BOLD, 16));
        lblNewLabel.setBounds(290, 30, 70, 15);
        panel.add(lblNewLabel);
        
        JLabel lblPassword = new JLabel("Password:");
        lblPassword.setFont(new Font("Dialog", Font.BOLD, 16));
        lblPassword.setBounds(5, 280, 100, 15);
        panel.add(lblPassword);
        
        txtPassword = new JTextField();
        txtPassword.setBounds(110, 271, 170, 25);
        panel.add(txtPassword);
        txtPassword.setColumns(10);
       
        
        JButton btnSave = new JButton("Save");
        btnSave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String name = txtName.getText();
                String address = txtAreaAddress.getText();
                String profile = txtAreaProfile.getText();
                String password = txtPassword.getText();
                try {
                    if (employee == null) {
                        Employee tmpEmp = hksystem.createEmployee(name, address, profile, password);
                        employees.add(tmpEmp);
                        empTableModel.setEmpTableModel(employees);
                        empTableModel.fireTableDataChanged();
                    } else {
                        employee.setEmployee(name, address, profile, password);
                        hksystem.merge(employee);
                        empTableModel.fireTableDataChanged();
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        });
        btnSave.setBounds(5, 424, 117, 25);
        panel.add(btnSave);
        
        JButton btnClose = new JButton("Close");
        btnClose.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        btnClose.setBounds(160, 424, 117, 25);
        panel.add(btnClose);
    }
    
    public void setEmpTableModel(EmployeeTableModel anEmpTableModel) {
        empTableModel = anEmpTableModel;
    }
    
    public void setHkSystem(HkSystem aHksystem) {
        hksystem = aHksystem;
    }
    
    public void setEmployees(List<Employee> theEmployees) {
        employees = theEmployees;
    }
    
    public void setEmployee(Employee anEmployee) {
        employee = anEmployee;
    }

    // SETTERS FOR TEXTFIELDS
    public void setTxtName(String aTxtName) {
        txtName.setText(aTxtName);
    }

    public void setTxtAreaAddress(String aTxtAreaAddress) {
        txtAreaAddress.setText(aTxtAreaAddress);
    }

    public void setTxtAreaProfile(String aTxtAreaProfile) {
        txtAreaProfile.setText(aTxtAreaProfile);
    }

    public void setTxtPassword(String aTxtPassword) {
        txtPassword.setText(aTxtPassword);
    }

    public void setLblWelcomeEmp(String aLblWelcomeEmp) {
        lblWelcomeEmp.setText(aLblWelcomeEmp);
    }
} // END OF EMPLOYEE FRAME CLASS

// EMAIL FRAME CLASS
class EmailFrame extends JFrame {

    private JPanel contentPane;
    private JLabel lblFrom;
    private JLabel lblTo;
    private JLabel lblSubject;
    private JTextArea textAreaMessage;
    private JLabel lblDate;
    private JButton btnClose;

    public EmailFrame() {
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setBounds(100, 100, 580, 654);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        
        lblFrom = new JLabel("From:  ");
        lblFrom.setBounds(12, 49, 332, 15);
        contentPane.add(lblFrom);
        
        lblTo = new JLabel("To:  ");
        lblTo.setBounds(12, 96, 332, 15);
        contentPane.add(lblTo);
        
        lblSubject = new JLabel("Subject:  ");
        lblSubject.setBounds(12, 170, 550, 15);
        contentPane.add(lblSubject);
        
        textAreaMessage = new JTextArea();
        textAreaMessage.setBounds(12, 203, 556, 320);
        textAreaMessage.setLineWrap(true);
        contentPane.add(textAreaMessage);
        
        lblDate = new JLabel("Date:  ");
        lblDate.setBounds(413, 96, 155, 15);
        contentPane.add(lblDate);
        
        btnClose = new JButton("Close");
        btnClose.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        btnClose.setBounds(12, 563, 117, 25);
        contentPane.add(btnClose);
    }

    public void setTxtLblFrom(String txtFrom) {
        lblFrom.setText("From:  " + txtFrom);
    }

    public void setTxtLblTo(String txtTo) {
        lblTo.setText("To:  " + txtTo);
    }

    public void setTxtLblSubject(String txtSubject) {
        lblSubject.setText("Subject:  " + txtSubject);
    }
    
    public void setLblDate(String txtDate) {
        lblDate.setText("Date:  " + txtDate);
    }

    public void setTextAreaMessage(String txtAreaMessage) {
        textAreaMessage.setText(txtAreaMessage);
    }

} // END OF EMAIL FRAME CLASS


// CONTRIBUTE FRAME CLASS
class ContributeFrame extends JFrame
{

    private JPanel contentPane;
    private JTextField txtAuthoriser;
    private Beenz latestBeenz;
    private Employee employee;
    private HkSystem hksystem;
    private String contributeType = "uploadDoc";

    public ContributeFrame()
    {
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        
        JLabel lblContribute = new JLabel("Contribute by:");
        lblContribute.setFont(new Font("Dialog", Font.BOLD, 14));
        lblContribute.setBounds(10, 20, 440, 15);
        contentPane.add(lblContribute);
        
        JRadioButton rdbtnUploadDoc = new JRadioButton("Upload Document (10 Beez)");
        rdbtnUploadDoc.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                contributeType = "uploadDoc";
            }
        });
        rdbtnUploadDoc.setSelected(true);
        rdbtnUploadDoc.setBounds(150, 16, 250, 23);
        contentPane.add(rdbtnUploadDoc);
        
        JRadioButton rdbtnOpenDoc = new JRadioButton("Open Document (5 Beez)");
        rdbtnOpenDoc.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                contributeType = "openDoc";
            }
        });
        rdbtnOpenDoc.setBounds(150, 50, 250, 23);
        contentPane.add(rdbtnOpenDoc);
        
        JRadioButton rdbtnContributeInfo = new JRadioButton("Add information (20 Beez)");
        rdbtnContributeInfo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                contributeType = "addInfo";
            }
        });
        rdbtnContributeInfo.setBounds(150, 84, 300, 23);
        contentPane.add(rdbtnContributeInfo);
        
        ButtonGroup group = new ButtonGroup();
        group.add(rdbtnUploadDoc);
        group.add(rdbtnOpenDoc);
        group.add(rdbtnContributeInfo);
        
        JLabel lblAuthorisedBy = new JLabel("Authorised by:");
        lblAuthorisedBy.setFont(new Font("Dialog", Font.BOLD, 14));
        lblAuthorisedBy.setBounds(10, 150, 120, 15);
        contentPane.add(lblAuthorisedBy);
        
        txtAuthoriser = new JTextField();
        txtAuthoriser.setFont(new Font("Dialog", Font.PLAIN, 14));
        txtAuthoriser.setBounds(150, 143, 150, 23);
        contentPane.add(txtAuthoriser);
        txtAuthoriser.setColumns(10);
        
        JButton btnNewButton = new JButton("Make Contribution");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int totalPoints;
                int pointsToAdd;
                if(contributeType.equals("uploadDoc")) {
                    pointsToAdd = 10;
                    totalPoints = latestBeenz.getPoints() + pointsToAdd;
                } else if (contributeType.equals("openDoc")) {
                    pointsToAdd = 5;
                    totalPoints = latestBeenz.getPoints() + pointsToAdd;
                    
                } else {    // ADD INFORMATION
                    pointsToAdd = 20;
                    totalPoints = latestBeenz.getPoints() + pointsToAdd;
                }
                String authoriser = txtAuthoriser.getText();
                if (authoriser.equals("")) {
                    JOptionPane.showMessageDialog(new JFrame(), "Please enter authoriser!");
                } else {
                    Beenz newBeenz = new Beenz(totalPoints, authoriser, new Date(), employee);
                    hksystem.createBeenz(newBeenz);
                    JOptionPane.showMessageDialog(new JFrame(), "Contribution successful " + pointsToAdd + " beenz points added!");
                    dispose();
                }
            }
        });
        btnNewButton.setBounds(10, 190, 200, 25);
        contentPane.add(btnNewButton);
    }
    
    public void setBeenz(Beenz aBeenz) {
        latestBeenz = aBeenz;
    }
    
    public void setEmployee(Employee anEmployee) {
        employee = anEmployee;
    }
    
    public void setHkSystem(HkSystem aHkSystem) {
        hksystem = aHkSystem;
    }
} // END OF CONTRIBUTE FRAME CLASS

// REDEEM FRAME CLASS
class RedeemFrame extends JFrame
{

    private JPanel contentPane;
    private JTextField txtAuthoriser;
    private Beenz latestBeenz;
    private Employee employee;
    private HkSystem hksystem;
    private String redeemType = "book";

    public RedeemFrame()
    {
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setBounds(100, 100, 450, 300);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        setContentPane(contentPane);
        contentPane.setLayout(null);
        
        JLabel lblBuy = new JLabel("Buy:");
        lblBuy.setFont(new Font("Dialog", Font.BOLD, 14));
        lblBuy.setBounds(10, 20, 440, 15);
        contentPane.add(lblBuy);
        
        JRadioButton rdbtnBook = new JRadioButton("Book (50 Beez)");
        rdbtnBook.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                redeemType = "book";
            }
        });
        rdbtnBook.setSelected(true);
        rdbtnBook.setBounds(150, 16, 250, 23);
        contentPane.add(rdbtnBook);
        
        JRadioButton rdbtnCD = new JRadioButton("CD (25 Beez)");
        rdbtnCD.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                redeemType = "cd";
            }
        });
        rdbtnCD.setBounds(150, 50, 250, 23);
        contentPane.add(rdbtnCD);
        
        JRadioButton rdbtnVacation = new JRadioButton("Vacation (200 Beez)");
        rdbtnVacation.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                redeemType = "vacation";
            }
        });
        rdbtnVacation.setBounds(150, 84, 300, 23);
        contentPane.add(rdbtnVacation);
        
        ButtonGroup group = new ButtonGroup();
        group.add(rdbtnBook);
        group.add(rdbtnCD);
        group.add(rdbtnVacation);
        
        JLabel lblAuthorisedBy = new JLabel("Authorised by:");
        lblAuthorisedBy.setFont(new Font("Dialog", Font.BOLD, 14));
        lblAuthorisedBy.setBounds(10, 150, 120, 15);
        contentPane.add(lblAuthorisedBy);
        
        txtAuthoriser = new JTextField();
        txtAuthoriser.setFont(new Font("Dialog", Font.PLAIN, 14));
        txtAuthoriser.setBounds(150, 143, 150, 23);
        contentPane.add(txtAuthoriser);
        txtAuthoriser.setColumns(10);
        
        JButton btnPurchase = new JButton("Purchase");
        btnPurchase.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int totalPoints;
                int pointsToDeduct;
                if(redeemType.equals("book")) {
                    pointsToDeduct = 50;
                    totalPoints = latestBeenz.getPoints() - pointsToDeduct;
                } else if (redeemType.equals("cd")) {
                    pointsToDeduct = 25;
                    totalPoints = latestBeenz.getPoints() - pointsToDeduct;
                    
                } else {    // ADD INFORMATION
                    pointsToDeduct = 200;
                    totalPoints = latestBeenz.getPoints() - pointsToDeduct;
                }
                String authoriser = txtAuthoriser.getText();
                if (authoriser.equals("")) {
                    JOptionPane.showMessageDialog(new JFrame(), "Please enter authoriser!");
                } else if (totalPoints < 0) {
                    JOptionPane.showMessageDialog(new JFrame(), "Insufficient beez points!");
                } else {
                    Beenz newBeenz = new Beenz(totalPoints, authoriser, new Date(), employee);
                    hksystem.createBeenz(newBeenz);
                    JOptionPane.showMessageDialog(new JFrame(), "Purchase successful " + pointsToDeduct + " beenz points deducted!");
                    dispose();
                }
            }
        });
        btnPurchase.setBounds(10, 190, 200, 25);
        contentPane.add(btnPurchase);
    }
    
    public void setBeenz(Beenz aBeenz) {
        latestBeenz = aBeenz;
    }
    
    public void setEmployee(Employee anEmployee) {
        employee = anEmployee;
    }
    
    public void setHkSystem(HkSystem aHkSystem) {
        hksystem = aHkSystem;
    }
} // REDEEM FRAME CLASS

